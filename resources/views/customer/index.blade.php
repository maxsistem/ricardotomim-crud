@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="card-title">
                        <a class="btn btn-success" href="/novo" role="button">Cadastrar Novo</a>
                    </div>                    
                    <table class="table">
                        <thead>
                          <tr>
                            <th scope="col">#</th>
                            <th scope="col">Nome</th>
                            <th scope="col">Nascimento</th>
                            <th scope="col">Sexo</th>
                            <th scope="col">Endereço completo</th>
                            <th scope="col"></th>
                          </tr>
                        </thead>
                        <tbody>
                            @foreach ($customers as $customer)
                                <tr>
                                    <th>{{$customer->id}}</th>
                                    <td>{{$customer->name}}</td>
                                    <td>{{ Carbon\Carbon::parse($customer->date_birth)->format('d/m/Y') }}</td>
                                    <td>{{$customer->gender}}</td>
                                    <td>
                                        @if ($customer->address_street)
                                            {{$customer->address_street}}, {{$customer->address_number}} {{$customer->address_complement}} - {{$customer->address_district}} - {{$customer->address_city}}/{{$customer->address_state}} - {{$customer->address_postalcode}}
                                        @endif
                                    </td>
                                    <td>
                                        <div class="btn-toolbar">
                                            <div class="btn-group mr-2" role="group" aria-label="Primeiro grupo">
                                                <a class="btn btn-primary btn-sm" href="/editar/{{$customer->id}}" role="button">Editar</a>
                                                <a class="btn btn-danger btn-sm" href="/excluir/{{$customer->id}}" role="button">Excluir</a>
                                            </div>
                                        </div>
                                    </td>                                    
                                </tr>
                            @endforeach                          
                        </tbody>
                      </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
