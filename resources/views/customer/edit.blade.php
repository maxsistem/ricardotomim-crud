@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <strong>{{$title}}</strong>
                </div>

                <div class="card-body">                                    
                    <form action="{{ $url_post }}" method="post">
                        @csrf
                        <div class="form-group row">
                            <label for="name" class="col-md-3 col-form-label">Nome completo*</label>
                            <div class="col-md-9">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name', $customer['name']) }}" required autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="date_birth" class="col-md-3 col-form-label">Data de nascimento*</label>
                            <div class="col-md-4">
                                <input id="date_birth" type="date" class="form-control @error('date_birth') is-invalid @enderror" name="date_birth" value="{{ old('date_birth', $customer['date_birth']) }}" required>

                                @error('date_birth')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="gender" class="col-md-3 col-form-label">Sexo*</label>
                            <div class="col-md-3">
                                <select id="gender" class="form-control @error('gender') is-invalid @enderror" name="gender" required>
                                    <option selected></option>
                                    <option value="M" {{ old('gender', $customer['gender']) == 'M' ? 'selected' : '' }}>Masculino</option>
                                    <option value="F" {{ old('gender', $customer['gender']) == 'F' ? 'selected' : '' }}>Feminino</option>
                                </select>

                                @error('gender')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="address_postalcode" class="col-md-3 col-form-label">Cep</label>
                            <div class="col-md-3">
                                <input id="address_postalcode" type="text" class="form-control @error('address_postalcode') is-invalid @enderror" name="address_postalcode" value="{{ old('address_postalcode', $customer['address_postalcode']) }}" maxlength="10">

                                @error('address_postalcode')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div> 
                        </div>
                        <div class="form-group row">
                            <label for="address_street" class="col-md-3 col-form-label">Endereço</label>
                            <div class="col-md-9">
                                <input id="address_street" type="text" class="form-control @error('address_street') is-invalid @enderror" name="address_street" value="{{ old('address_street', $customer['address_street']) }}">

                                @error('address_street')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="address_number" class="col-md-3 col-form-label">Número</label>
                            <div class="col-md-3">
                                <input id="address_number" type="text" class="form-control @error('address_number') is-invalid @enderror" name="address_number" value="{{ old('address_number', $customer['address_number']) }}">

                                @error('address_number')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="address_complement" class="col-md-3 col-form-label">Complemento</label>
                            <div class="col-md-6">
                                <input id="address_complement" type="text" class="form-control @error('address_complement') is-invalid @enderror" name="address_complement" value="{{ old('address_complement', $customer['address_complement']) }}">

                                @error('address_complement')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="address_district" class="col-md-3 col-form-label">Bairro</label>
                            <div class="col-md-8">
                                <input id="address_district" type="text" class="form-control @error('address_district') is-invalid @enderror" name="address_district" value="{{ old('address_district', $customer['address_district']) }}">

                                @error('address_district')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="address_city" class="col-md-3 col-form-label">Cidade</label>
                            <div class="col-md-9">
                                <input id="address_city" type="text" class="form-control @error('address_city') is-invalid @enderror" name="address_city" value="{{ old('address_city', $customer['address_city']) }}"  readonly>

                                @error('address_city')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="address_state" class="col-md-3 col-form-label">UF</label>
                            <div class="col-md-2">
                                <input id="address_state" type="text" class="form-control @error('address_state') is-invalid @enderror" name="address_state" value="{{ old('address_state', $customer['address_state']) }}" readonly>

                                @error('address_state')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-3">
                                <button type="submit" class="btn btn-primary">
                                    Salvar
                                </button>
                            </div>
                            <div class="col-md-3">
                                <a class="btn btn-link" href="/" role="button">Cancelar</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" >

    $(document).ready(function() {
       
        function clearData() {
            $("#address_street").val("");
            $("#address_district").val("");
            $("#address_city").val("");
            $("#address_state").val("");
        }        
        
        $("#address_postalcode").blur(function() {

            const postalcode = $(this).val().replace(/\D/g, '');

            if (postalcode != "") {

                const validate = /^[0-9]{8}$/;

                if(validate.test(postalcode)) {

                    $("#address_street").val("buscando...");

                    $.getJSON("https://viacep.com.br/ws/"+ postalcode +"/json/?callback=?", function (res) {

                        if (!("erro" in res)) {
                            $("#address_street").val(res.logradouro);
                            $("#address_district").val(res.bairro);
                            $("#address_city").val(res.localidade);
                            $("#address_state").val(res.uf);
                        } else {
                            clearData();
                            alert("CEP não encontrado.");
                        }
                    });
                } else {
                    clearData();
                    alert("CEP inválido.");
                }
            } else {
                clearData();
            }
        });
    });

</script>
@endsection
