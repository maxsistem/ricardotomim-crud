<?php

namespace App\Http\Controllers;


use App\Repositories\CustomerRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CustomerController extends Controller
{
    private $customerRepository;

    public function __construct(CustomerRepository $customerRepository)
    {
        $this->customerRepository = $customerRepository;
    }

    public function index()
    {
        $data['customers'] = $this->customerRepository->getAll();

        return view('customer.index', $data);
    }


    /**
     * View create
     *
     * @return void
     */
    public function add()
    {   
        $customer =  [
            'name' => null,
            'date_birth' => null,
            'gender' => null,
            'address_street' => null,
            'address_number' => null,
            'address_complement' => null,
            'address_district' => null,
            'address_city' => null,
            'address_state' => null,
            'address_postalcode' => null,
        ];


        $data = [
            'title' => 'Cadastrar novo cliente',
            'url_post' => '/novo',
            'customer' => $customer
        ];
        return view('customer.edit', $data);
    }
    
    /**
     * View update
     *
     * @param [type] $id
     * @return void
     */
    public function edit(int $id)
    {
        $customer = $this->customerRepository->findById($id);
        
        $data = [
            'title' => 'Editar cliente: ' . $customer->name,
            'url_post' => '/editar/' . $id,
            'customer' => $customer
        ];
        return view('customer.edit', $data);
    }


    /**
     * Create
     *
     * @param Request $request
     * @return void
     */
    public function create(Request $request)
    {
       
        $data = $request->all();

        $validator = $this->validator($data);

        if ($validator->fails()) {
            return redirect('novo')
                        ->withErrors($validator)
                        ->withInput();
        }

        $this->customerRepository->create($data);

        return redirect('/');
    }


    /**
     * Update
     *
     * @param Request $request
     * @param [type] $id
     * @return void
     */
    public function update(Request $request, $id)
    {
       
        $data = $request->all();

        $validator = $this->validator($data);

        if ($validator->fails()) {
            return redirect('editar/' . $id)
                        ->withErrors($validator)
                        ->withInput();
        }

        $this->customerRepository->update($id, $data);

        return redirect('/');
    }


    /**
     * Delete
     *
     * @param [type] $id
     * @return void
     */
    public function delete($id)
    {
        $this->customerRepository->delete($id);
        return redirect('/');
    }


    /**
     *
     * @param [type] $data
     * @return void
     */
    private function validator($data)
    {
        $validator = Validator::make($data, [
            'name' => 'required|max:255',
            'date_birth' => 'required',
            'gender' => 'required',
            'address_street' => 'max:255',
            'address_number' => 'max:20',
            'address_complement' => 'max:30',
            'address_district' => 'max:50',
            'address_city' => 'max:100',
            'address_state' => 'max:2',
            'address_postalcode' => 'max:10',
        ]);

        return $validator;
    }

}
