<?php

namespace App\Repositories;

use App\Customer;

Class CustomerRepository
{
    private $customer;

    public function __construct(Customer $customer)
    {
        $this->customer = $customer;
    }

    /**
     *
     * List
     * 
     * @param boolean $pagination
     * @param integer $take
     * @return void
     */
    public function getAll($pagination = false, $take = 15)
    {
        if($pagination) {
            return $this->customer->pagination($take);
        }

        return $this->customer->all();
    }

    /**
     * Find
     *
     * @param integer $id
     * @return void
     */
    public function findById(int $id)
    {
        return $this->customer->find($id);
    }


    /**
     * Create
     *
     * @param array $data
     * @return void
     */
    public function create(array $data)
    {
        return $this->customer->create($data);
    }

    /**
     * Update
     *
     * @param integer $id
     * @param array $data
     * @return void
     */
    public function update(int $id, array $data)
    {
        $customer = $this->customer->find($id);
        $customer->fill($data);

        return $customer->save();
    }


    /**
     * Delete
     *
     * @param integer $id
     * @return void
     */
    public function delete(int $id)
    {
        $customer = $this->customer->find($id);
        return $customer->delete();
    }
}