<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'CustomerController@index');

Route::get('/novo', 'CustomerController@add');
Route::post('/novo', 'CustomerController@create');

Route::get('/editar/{id}', 'CustomerController@edit');
Route::post('/editar/{id}', 'CustomerController@update');

Route::get('/excluir/{id}', 'CustomerController@delete');





Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
